﻿using System;

namespace comp5002_10013028_assessment01
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Clear();
        
            string name, selection; 
            double decimalnumber,gst;
            gst = 0.15;
            

            Console.WriteLine("*******************"); 
            Console.WriteLine("*******************");
            Console.WriteLine("*      Welcome    *");
            Console.WriteLine("*      To The     *");   /* Welcome To The Ian's Shop */
            Console.WriteLine("*    Ian's Shop!  *");
            Console.WriteLine("*******************");
            Console.WriteLine("*******************");

           
            Console.WriteLine("< Please Enter your name >"); /* Please Enter your name */
            
            name = Console.ReadLine();

            Console.WriteLine("Welcome " + (name));

             
            Console.WriteLine("Please Type a Number With 2 decimals"); /* Please Type a number with 2 decimals */

            decimalnumber = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Would you like to add another number?"); /* Would you like to add another number? */

            Console.WriteLine("If yes then select <1>, If No then select <2>"); /* If yes then select <1>, If no then select <2> */
            Console.WriteLine("1");
            Console.WriteLine("2");

            selection = Console.ReadLine();
        
             if (selection == "1")
              
            { 
                Console.WriteLine(" Please add your other number"); /*Please add your other number */
              decimalnumber += Convert.ToDouble(Console.ReadLine()); 
            }
        
               
                 
                 Console.WriteLine($"total  = {decimalnumber+(decimalnumber * gst)}"); /*The total */
                 
                 Console.WriteLine("Thank you for shopping with us"); /*Thank you for shopping with us */
                 Console.ResetColor();
                Console.WriteLine();
                 Console.WriteLine($"Press <Enter> to end program"); /*Press the enter button to end the program */
                 Console.ReadKey();
        }
    }
}